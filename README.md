# X-Interview

## Digital Currency

### Write a function by JavaScript that calculates the maximum profit of an investor in a day.

-	The investors buy digital currency and sell on the same day.
-	Only 2 transactions are allowed in a day (note: 2nd transaction can only start when 1st transaction is completed)

#### Examples:


Input:   prices[] = {`11`, `23`, `6`, `70`, `60`, `85`}\
Output:  `91`\
Investor earns `91` as sum of `12`, `79`\
Buy at `11`, sell at `23`,Buy at `6`and sell at `85`



Input:   prices[] = {`3`, `20`, `15`, `10`, `7`, `25`, `20`, `30`}\
Output:  `40`\
Investor earns `40` as sum of `17` and `23`\
Buy at `3`, sell at `20`, buy at `7` and sell at `30`



Input:   prices[] = { `77`, `60`, `12`, `12`, `10`, `50`, `55`}\
Output:  `45`\
Buy at `10` and sell at `55`.



Input:   prices[] = {`85`, `75`, `70`, `66`, `10`}\
Output:  `0`\
Not possible to earn.
