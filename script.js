var trn = 0;
var netprofit = 0;
var itrate = 0;
var inputlength = 0;
function reset() {
  trn = 0;
  netprofit = 0;
  itrate = 0;
  inputlength = 0;
}
function calculateMaxProfit(data) {
  itrate++;
  if (trn == 2) return;
  if (!data) return;

  var input = data.replace(/\s+/g, "");
  input = input.split(",");

  if (itrate == 1) {
    $("#profit").html("");
    $("#trn").html("");
    inputlength = input.length;
  }
  var buy;
  var sell;
  var profit;
  buy = input.shift() - 0;
  sell = input.shift() - 0;
  console.log(itrate);
  if (sell > buy && itrate == 1) {
    profit = sell - buy;
    netprofit = netprofit + profit;

    $("#profit").html(netprofit);
    $("#trn").append(
      "Buy at " + buy + " Sell at " + sell + " Profit " + profit + "<br />"
    );
    trn++;
  } else if (sell > buy) {
    console.log(input);
    for (i = 0; i < input.length; i++) {
      if (sell < input[i]) {
        sell = input[i] - 0;
        break;
      }
    }

    if (sell > buy && trn < 2) {
      profit = sell - buy;
      netprofit = netprofit + profit;

      $("#profit").html(netprofit);
      trn++;
      $("#trn").append(
        "Buy at " + buy + " Sell at " + sell + " Profit " + profit + "<br />"
      );
    }
  }
  calculateMaxProfit(input.join());
  console.log(inputlength);
  if (trn == 0 && inputlength - 2 == input.length) {
    $("#profit").html(netprofit);
    $("#trn").append("Not possible to earn ");
  }
}